"use strict";

const Koa = require("koa");
const model = require("./models");
const app = new Koa();

app.use(async (ctx, next) => {
  // 此处，应该在初始化数据库成功后，才进行插入数据的尝试
  await model.sync().then((res) => {
    let User = model.User;
    let Msg = model.Message;
    User.create({ username: '狄仁杰', password: '123456' });
    User.create({ username: '李元芳', password: '123456' });
    User.create({ username: '鲁班七号', password: '123456' });
    User.create({ username: '虞姬', password: '123456' });
    User.create({ username: '后弈', password: '123456' });
    User.create({ username: '盘古', password: '123456' });
    User.create({ username: '孙悟空', password: '123456' });

    Msg.create({ fromUserId: 1, content: '哎，今天天气不错，找个小贼下酒' })
  });
  await next();
  // let User=model.User;
  // console.log(User===UU);
  // let u1=await UU.create({username:'admin',password:'123456'});
  // let u2=await User.create({username:'admin',password:'123456'});
});
app.use(async (ctx, next) => {
  let User = model.User;
  let listUser = await User.findAll();
  console.log(JSON.stringify(listUser));
  ctx.body = "你好，李焕英";
});

const port = 3000;
app.listen(port);

console.log(`http://localhost:${port}`);
