const { sequelize, defineModel, DataTypes } = require('../db');

let obj = defineModel('messages', {
    fromUserId: {
        type: DataTypes.BIGINT,
        allowNull: false
    },
    toUserId: {
        type: DataTypes.BIGINT,
        allowNull: true
    },
    content: {
        type: DataTypes.STRING(80),
        allowNull: false
    }
});

module.exports = obj;