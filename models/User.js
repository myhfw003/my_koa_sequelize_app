const { sequelize, defineModel, DataTypes } = require('../db');

let User = defineModel('users', {
  username: {
    type: DataTypes.STRING(80),
    allowNull: false
  },
  password: {
    type: DataTypes.STRING(80),
    allowNull: false
  }
});
console.log(User);

module.exports = User;